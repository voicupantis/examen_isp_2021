package s1;

import java.util.concurrent.TimeUnit;

class PrintSequenceRunnable implements Runnable{

    public int top=9;
    static int  nr=1;
    int remainder;
    static Object lock=new Object();

    PrintSequenceRunnable(int remainder)
    {
        this.remainder=remainder;
    }

    @Override
    public void run() {
        while (nr < top-1) {
            synchronized (lock) {
                while (nr % 3 != remainder) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName() + " " + nr);
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                nr++;
                lock.notifyAll();
            }
        }
    }
}
public class PrintThreadsSequentiallyMain {

    public static void main(String[] args) {

        PrintSequenceRunnable runnable1=new PrintSequenceRunnable(1);
        PrintSequenceRunnable runnable2=new PrintSequenceRunnable(2);
        PrintSequenceRunnable runnable3=new PrintSequenceRunnable(0);
        Thread NiceThread1=new Thread(runnable1,"NiceThread1");
        Thread NiceThread2=new Thread(runnable2,"NiceThread2");
        Thread NiceThread3=new Thread(runnable3,"NiceThread3");
        NiceThread1.start();
        NiceThread2.start();
        NiceThread3.start();
    }
}